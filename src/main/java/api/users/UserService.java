package api.users;

import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;


public class UserService {
    private static List<User> returnUsersFromJson() {
        java.util.List<User> users = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("src/main/data/users.json")) {
            Object obj = jsonParser.parse(reader);
            JSONArray userList = (JSONArray) obj;
            System.out.println("json users");
            System.out.println(userList);


            userList.forEach(user -> {
                User currentUser = parseUserObject((JSONObject) user);
                users.add(currentUser);
            });


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("returned users");
        System.out.println(users);
        return users;
    }

    private static User parseUserObject(JSONObject user) {
        JSONObject userObject = (JSONObject) user.get("user");

        String id = (String) userObject.get("id");
        System.out.println(id);

        String firstName = (String) userObject.get("firstName");
        System.out.println(firstName);

        String lastName = (String) userObject.get("lastName");
        System.out.println(lastName);

        String username = (String) userObject.get("username");
        System.out.println(username);

        String email = (String) userObject.get("email");
        System.out.println(email);

        User newUser = new User(id, firstName, lastName, username, email);

        return newUser;
    }

    private static JSONObject writeNewUserData(User newUser, String mode) {
        List<User> usersFromJson = returnUsersFromJson();

        boolean updatingUsers = false;

        for (User user : usersFromJson) {
            if (user.getId().equals(newUser.getId())) {
                updatingUsers = true;
                if (mode == "update") {
                    user.setUsername(newUser.getUsername());
                    user.setFirstName(newUser.getFirstName());
                    user.setLastName(newUser.getLastName());
                    user.setEmail(newUser.getEmail());
                }
                if (mode == "delete") {
                    usersFromJson.remove(newUser);
                }
            }
        }

        if (!updatingUsers) {
            usersFromJson.add(newUser);
        }

        JSONArray newUsersJson = new JSONArray();

        for (User user : usersFromJson) {
            JSONObject newUserAttributesJson = new JSONObject() {{
                put("id", user.getId());
                put("firstName", user.getFirstName());
                put("lastName", user.getLastName());
                put("username", user.getUsername());
                put("email", user.getEmail());
            }};

            JSONObject newUserJson = new JSONObject() {{
                put("user", newUserAttributesJson);
            }};

            newUsersJson.add(newUserJson);
        }

        try (FileWriter file = new FileWriter("src/main/data/users.json")) {
            String jsonString = newUsersJson.toJSONString();
            file.append(jsonString);
            file.flush();
            return (JSONObject) newUsersJson.get(newUsersJson.size() - 1);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<User> getAllUsers() {
        return returnUsersFromJson();
    }

    // returns a single api.user by id
    public User getUser(String id) {
        List<User> usersFromJson = returnUsersFromJson();
        for (User user : usersFromJson) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }

    public JSONObject createUser(User newUser) {
        return writeNewUserData(newUser, "create");
    }

    public JSONObject updateUser(User updatedUser) {
        List<User> usersFromJson = returnUsersFromJson();
        User foundUser = null;
        for (User user : usersFromJson) {
            if (user.getId().equals(updatedUser.getId())) {
                foundUser = user;
            }
        }

        if (foundUser == null) {
            return null;
        } else {
            return writeNewUserData(updatedUser, "update");
        }
    }

    public JSONObject deleteUser(String id) {
        List<User> usersFromJson = returnUsersFromJson();
        User foundUser = null;
        for (User user : usersFromJson) {
            if (user.getId().equals(id)) {
                foundUser = user;
            }
        }

        if (foundUser == null) {
            return null;
        } else {
            return writeNewUserData(foundUser, "delete");
        }
    }
}
