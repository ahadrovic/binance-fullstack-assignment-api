package api.users;

import api.utils.ResponseError;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;
import java.util.List;

import static spark.Spark.*;
import static api.utils.JsonUtil.*;

public class UserController {
    public UserController(final UserService userService) {
        get("/users", (req, res) -> {
            res.type("application/json");

            List<User> users = userService.getAllUsers();

            if (users.isEmpty() || users == null) {
                res.status(400);
                return new ResponseError("No users found");
            }

            return users;

        }, json());

        get("/users/:id", (req, res) -> {
            res.type("application/json");

            String id = req.params(":id");
            User user = userService.getUser(id);
            if (user != null) {
                return user;
            }
            res.status(400);
            return new ResponseError("No user with id '%s' found", id);
        }, json());

        post("/users", (req, res) -> {
            res.type("application/json");
            User newUser = new Gson().fromJson(req.body(), User.class);
            System.out.println("new user");
            System.out.println(newUser);

            JSONObject newUserJson = userService.createUser(newUser);
            if (newUserJson == null) {
                res.status(500);
                return new ResponseError("The user could not be created");
            }

            res.status(200);
            return newUser;
        }, json());

        post("/users/:id", (req, res) -> {
            res.type("application/json");

            String id = req.params(":id");
            User updatedUser = new Gson().fromJson(req.body(), User.class);
            updatedUser.setId(id);

            System.out.println("updated user");
            System.out.println(updatedUser);

            JSONObject updateUserJson = userService.updateUser(updatedUser);
            if (updateUserJson == null) {
                res.status(500);
                return new ResponseError("The user was not found");
            }

            res.status(200);
            return updatedUser;
        }, json());

        delete("/users/:id", (req, res) -> {
            res.type("application/json");
            String id = req.params(":id");

            JSONObject updateUserJson = userService.deleteUser(id);

            if (updateUserJson == null) {
                res.status(500);
                return new ResponseError("The user to be deleted was not found");
            }

            res.status(200);
            return updateUserJson;
        }, json());

        exception(IllegalArgumentException.class, (e, req, res) -> {
            res.status(400);
            res.body(toJson(new ResponseError(e)));
        });
    }
}
