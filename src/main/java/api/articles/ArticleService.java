package api.articles;

import com.google.gson.Gson;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ArticleService {
    private static List<Article> returnArticlesFromJson() {
        List<Article> articles = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("src/main/data/articles.json")) {
            Object obj = jsonParser.parse(reader);
            JSONArray articleList = (JSONArray) obj;
            System.out.println("json articles");
            System.out.println(articleList);


            articleList.forEach(article -> {
                Article currentArticle = parseUserObject((JSONObject) article);
                articles.add(currentArticle);
            });


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("returned articles");
        System.out.println(articles);
        return articles;
    }

    private static Article parseUserObject(JSONObject article) {
        JSONObject articleObject = (JSONObject) article.get("article");

        String id = (String) articleObject.get("id");
        String title = (String) articleObject.get("title");
        String description = (String) articleObject.get("description");
        String category = (String) articleObject.get("category");
        String slug = (String) articleObject.get("slug");
        String language = (String) articleObject.get("language");
        String author = (String) articleObject.get("author");
        String content = (String) articleObject.get("content");
        String imageUrl = (String) articleObject.get("imageUrl");
        String videoUrl = (String) articleObject.get("videoUrl");
        long dateCreated = (long) articleObject.get("dateCreated");
        long dateModified = (long) articleObject.get("dateModified");
        long datePublished = (long) articleObject.get("datePublished");


        Article newArticle = new Article(id, title, description, category, slug, language, author, content, imageUrl, videoUrl, dateCreated, dateModified, datePublished);

        return newArticle;
    }

    private static JSONObject writeNewArticleData(Article newArticle, String mode) {
        List<Article> articlesFromJson = returnArticlesFromJson();
        Article foundArticle = null;
        boolean updatedArticles = false;

        for (Article article : articlesFromJson) {
            if (article.getId().equals(newArticle.getId())) {
                updatedArticles = true;
                if (mode == "update") {
                    article.setTitle(newArticle.getTitle());
                    article.setDescription(newArticle.getDescription());
                    article.setCategory(newArticle.getCategory());
                    article.setSlug(newArticle.getSlug());
                    article.setLanguage(newArticle.getLanguage());
                    article.setAuthor(newArticle.getAuthor());
                    article.setContent(newArticle.getContent());
                    article.setImageUrl(newArticle.getImageUrl());
                    article.setVideoUrl(newArticle.getVideoUrl());
                    article.setDateModified(newArticle.getDateModified());
                    article.setDatePublished(newArticle.getDatePublished());
                }
                foundArticle = article;
            }
        }

        if (!updatedArticles) {
            articlesFromJson.add(newArticle);
        } else {
            if (mode == "delete") {
                articlesFromJson.remove(foundArticle);
            }
        }

        JSONArray newArticlesJson = new JSONArray();

        for (Article article : articlesFromJson) {
            JSONObject newArticleAttributesJson = new JSONObject() {{
                put("id", article.getId());
                put("title", article.getTitle());
                put("description", article.getDescription());
                put("category", article.getCategory());
                put("slug", article.getSlug());
                put("language", article.getLanguage());
                put("author", article.getAuthor());
                put("content", article.getContent());
                put("imageUrl", article.getImageUrl());
                put("videoUrl", article.getVideoUrl());
                put("dateCreated", article.getDateCreated());
                put("dateModified", article.getDateModified());
                put("datePublished", article.getDatePublished());
            }};

            JSONObject newArticles = new JSONObject() {{
                put("article", newArticleAttributesJson);
            }};

            newArticlesJson.add(newArticles);
        }

        try (FileWriter file = new FileWriter("src/main/data/articles.json")) {
            String jsonString = newArticlesJson.toJSONString();
            file.append(jsonString);
            file.flush();
            return (JSONObject) newArticlesJson.get(newArticlesJson.size() - 1);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Article> getAllArticles() {
        return returnArticlesFromJson();
    }

    public Article getArticle(String id) {
        List<Article> articlesFromJson = returnArticlesFromJson();
        for (Article article : articlesFromJson) {
            if (article.getId().equals(id)) {
                return article;
            }
        }
        return null;
    }

    public JSONObject createArticle(Article newArticle) {
        return writeNewArticleData(newArticle, "create");
    }

    public JSONObject updateArticle(Article updatedArticle) {
        List<Article> articlesFromJson = returnArticlesFromJson();
        Article foundArticle = null;
        for (Article article : articlesFromJson) {
            if (article.getId().equals(updatedArticle.getId())) {
                foundArticle = article;
            }
        }

        if (foundArticle == null) {
            return null;
        } else {
            return writeNewArticleData(updatedArticle, "update");
        }
    }

    public JSONObject deleteArticle(String id) {
        try {
            List<Article> articlesFromJson = returnArticlesFromJson();
            Article foundArticle = null;

            if (articlesFromJson.size() > 0) {
                for (Article article : articlesFromJson) {
                    if (article.getId().equals(id)) {
                        foundArticle = article;
                    }
                }
            }

            if (foundArticle == null) {
                return new JSONObject() {{
                    put("statusCode", 200);
                }};
            } else {
                return writeNewArticleData(foundArticle, "delete");
            }
        } catch (Exception exception) {
            System.out.println(exception);
            return null;
        }
    }
}
