package api.articles;

import api.utils.ResponseError;
import com.google.gson.Gson;
import org.json.simple.JSONObject;

import java.util.List;

import static api.utils.JsonUtil.json;
import static api.utils.JsonUtil.toJson;
import static spark.Spark.*;

public class ArticleController {
    public ArticleController(final ArticleService articleService) {
        get("/articles", (req, res) -> {
            res.type("application/json");

            List<Article> articles = articleService.getAllArticles();

            if (articles.isEmpty() || articles == null) {
                res.status(400);
                return new ResponseError("No api.articles found");
            }

            return articles;

        }, json());

        get("/articles/:id", (req, res) -> {
            res.type("application/json");

            String id = req.params(":id");
            Article article = articleService.getArticle(id);
            if (article != null) {
                return article;
            }
            res.status(400);
            return new ResponseError("No article with id '%s' found", id);
        }, json());

        post("/articles", (req, res) -> {
            res.type("application/json");
            Article newArticle = new Gson().fromJson(req.body(), Article.class);
            System.out.println("new article");
            System.out.println(newArticle);

            JSONObject newArticleJson = articleService.createArticle(newArticle);
            if (newArticleJson == null) {
                res.status(500);
                return new ResponseError("The article could not be created");
            }

            res.status(200);
            return newArticle;
        }, json());

        post("/articles/:id", (req, res) -> {
            res.type("application/json");

            String id = req.params(":id");
            Article updatedArticle = new Gson().fromJson(req.body(), Article.class);
            updatedArticle.setId(id);

            System.out.println("updated article");
            System.out.println(updatedArticle);

            JSONObject updateArticleJson = articleService.updateArticle(updatedArticle);
            if (updateArticleJson == null) {
                res.status(500);
                return new ResponseError("The article was not found");
            }

            res.status(200);
            return updatedArticle;
        }, json());

        delete("/articles/:id", (req, res) -> {
            res.type("application/json");
            String id = req.params(":id");

            JSONObject deleteArticleJson = articleService.deleteArticle(id);

            if (deleteArticleJson == null) {
                res.status(500);
                return new ResponseError("The article to be deleted was not found");
            }
            res.status(200);
            return deleteArticleJson;
        }, json());

        exception(IllegalArgumentException.class, (e, req, res) -> {
            res.status(400);
            res.body(toJson(new ResponseError(e)));
        });
    }
}
