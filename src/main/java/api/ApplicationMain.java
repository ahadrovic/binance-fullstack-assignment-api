package api;

import api.articles.ArticleController;
import api.articles.ArticleService;
import org.apache.log4j.Logger;
import api.utils.SparkUtils;
import api.users.*;

import static spark.Spark.*;

public class ApplicationMain {



    public static void main(String[] args) {
        Logger logger = Logger.getLogger(ApplicationMain.class);
        SparkUtils.createServerWithRequestLog(logger);
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));
        path("/api", () -> {
            new UserController(new UserService());
            new ArticleController(new ArticleService());
        });

    }

}