# binance-fullstack-assignment-api

Clone or download this repository, run the project through Application Main
This project was built using Maven. 

The source code consists of a RESTful API for articles that can be created, read, 
updated, and deleted from the CMS demo's frontend (or a client like Postman)

This project was built using Spark Java (http://sparkjava.com/) a micro web framework

A few things to note about this project:

There is no database. Instead, all data is stored using a mechanism similar to flat-file CMS. 
All relevant data is stored in JSON files. This has been chosen because a proper setup and proper security
could not be implemented without the the use of paid 3rd party services.
To add, I also chose this due to the fact that this project is to
be treated as a demo that demonstrates RESTful API concepts in practice.

This project is only meant to be run locally. Because of this, HTTPS was not configured.



